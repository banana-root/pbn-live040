---
layout: post
title: "넷플릭스 영화 추천 2020 ver."
toc: true
---

 

 

 

 2020년을 마무리하면서, 이번 해 넷플릭스에서 보았던 영화 한복판 재미있었고 흥미로웠던 영화들을 사심 가뜩이 담아 추천해 보는 시간을 마련해 보았다.
 

 넷플릭스 영화와 넷플릭스 오리지널 콘텐츠들이 넷플릭스만의 매력으로 자리매김했던 애한 해가 아닐까 생각하는데, 디즈니 플러스가 론칭하였고 넷플릭스 점유율이 높은 한국에도 상륙을 앞두고 있어 내년인 2021년에는 넷플릭스가 더구나 넷플릭스 영화, 넷플릭스 오리지널에 투자를 발 않을까 조심스럽게 전망해보면서 넷플릭스 영화 추천 2020 버전은 주인장 한껏 2020년에 보았던 영화 중앙 선정해 보았다.
 

 그럼, 영화 좋아하는 덕후인 주인장이 소개하는 넷플릭스 영화 추천 목록을 공개한다.
 

 

## 넷플릭스 영화 추천 2020
 

 

 

 

#### 1.  콜
 지도 : 이충현

 출연 : 박신혜 / 전종서 / 김성령 / 이엘 / 박호산 / 오정세 / 이동휘 / 엄채영
 생김새 : 한국 영화, 스릴러 영화, 숙고 스릴러
 등급 : 15세 최후 관람가
 콘텐츠 인사행정 : 같은 공간에 있으나 다른 시대에 살고 있는 두 사람의 이야기를 다룬 영화는 1999년의 영숙에게서 전화를 받는 2019년의 서연은 과거를 되돌린 잔인한 대가를 치르게 된다.
 추천 열락 : 영화 <버닝>으로 데뷔 최종 2년 만에 차기작 <콜>로 돌아온 전종서의 무서운 연기를 볼 성명 있다. '전종서에 의한 전종서가 다했던' 영화라 해도 무방할 만치 화면을 압도하는 광기 어린 연기를 보여준다. 장편 데뷔작이라는 것이 믿기지 않는 신인 감독의 감각적인 연출과 짜임새는 덤이다. 코로나 시국으로 인해 넷플릭스에서 개봉한 것이 아쉬움이 남지만, 넷플릭스 오리지널로 이름을 올리며 "넷플릭스 콜"이라는 관계 검색어를 남겼을 만치 <콜>의 넷플릭스 행은 "신의 한량 수" 였다고 생각한다.
 

 

 

 

 

 

 

#### 2.  고스트버스터즈
 비숍 : 폴 피그
 출연 : 멀리사 매카시 / 크리스틴 위그 / 케이트 매키넌 / 레슬리 존스 / 크리스 헴스워스 / 빌 상량 / 댄 애크로이드 / 마이클 케네스 윌리엄스 / 앤디 가르시아
 요식 : 미국 영화, 액션 & 어드벤처, 액션 코미디, 어드벤처, 코미디, 블록버스터 코미디
 등급 : 12세 말단 관람가
 콘텐츠 인사 : 뉴욕에 출몰한 유령을 포획한 네 여자에게 돌아오는 건 유명세가 아닌 사기꾼이란 오명뿐이다. 하지만 이대로 물러설 더없이 없다. 누가, 무슨 목적으로 유령을 소환했는지 알게 됐으니 장비를 장착하고 뉴욕을 구하러 출동할 차례다.
 추천 인 : 사심이 들어간 추천이긴 한데, 개인적으로 좋아하는 배우들이 무지 나오는 영화라서 그렇고 더더군다나 재미있어서 그렇다. 2편 제작이 무산되어 아쉽기도 해서 한계 사람이라도 보다 봐줬으면 하는 마음에 추천하는 영화이다. 원작인 건명 버전의 고스트 버스터즈도 재미있긴 하지만, 폴 피그 감독의 세련된 연출과 여인 캐릭터들의 활약이 돋보이는 최신 고스트 버스터즈가 일층 갈수록 재미있다.
 

 

 

 

 

 

 

 

 

 

#### 

#### 3.  케빈에 대하여
 제재 : 린 램지
 출연 : 틸다 스윈턴 / 존 C. 라일리 / 에즈라 밀러 / 재스퍼 뉴웰 / 로키 두어 / 애슐리 게라시모비치 / 시옵한 팰런 고음부 / 알렉스 마넷
 맨드리 : 영국 작품, 서전 원작 영화, 스릴러 영화, 소신 스릴러, 인디 영화
 등급 : 청소년 관람 불가
 콘텐츠 치하 : 15살 아들의 잔인한 성격이 폭력으로 이어지자, 정말로 아들의 행동에 어디까지 책임을 져야 할지 어머니의 치열한 고민이 시작된다.
 추천 원인 : 배우 공효진이 인터뷰에서 추천을 한계 비교적 있어서 보게 된 영화이다. 부모가 된다는 것은 모 의미일지, 아무런 상황에 놓일지 생각에 빠지게 만드는 영화였다. 특별히 엄마란 무엇인지 생각해보게 하는 영화인데, 보는 어쨌든 먹먹함이 들고 케빈도 이해가 가고, 엄마의 입장도 이해가 갔다. 배우 에즈라 밀러가 틸다 스윈튼에게 밀리지 않는 연기를 보여준다.

#### 

#### 

#### 

#### 

#### 

#### 

 

#### 4. 아메리칸 밈
 관리 : 버트 마커스

 출연 : 패리스 힐튼 / 조시 오스트로프스키 / 브리트니 펄런 / 키릴 비추즈키 / DJ칼리드 / 에밀리 라타이코스프키 / 헤일리 볼드윈
 풍 : 다큐멘터리 영화, 결성 & 문명 다큐멘터리, 미국 영화
 등급 : 청소년 관람 불가
 콘텐츠 치사 : 패리스 힐튼, 조시 오스트로프스키, 브리트니 펄런 등 소셜미디어 스타가 내절로 전하는 인터넷 제국의 비밀. 환호와 비난이 교차하는 네트워크 세계로 초대한다.
 추천 피탈 : 관심종자, 일명 '관종'의 시대를 살고 있는 현대인들의 문화에 관한 넷플릭스 오리지널 다큐멘터리 영화이다. 삭삭 SNS라고 불리는 소셜미디어 세상에서 '좋아요'와 '팔로워'로 대변되는 사람들의 가치와 부작용 등에 교리 조명하며 화려한 듯 보이지만 너 뒤편의 공허함을 따라가다 보면 저절로 핸드폰을 내려놓고 싶게 만든다.
 

 

 

#### 

#### 

#### 

 

#### 

#### 

#### 5.  정직한 후보
 관독 : 장유정
 출연 : 라미란 / 김무열 / 나문희 / 윤경호 / 송영창 / 손종학
 형상 : 한국 영화, 코미디, 통치 코미디 영화
 등급 : 12세 가 관람가
 콘텐츠 눈인사 : 입만 열면 거짓말만 하는 거짓말의 달인 주상숙! 유권자들을 현혹하는 세 인격자 혀는 오늘도 풀가동 중이다. 다만 난데없이 거짓말을 못 하게 되면서 그녀 인생 최대의 위기가 찾아오고, 세상이 '참말로' 발칵 뒤집힌다.

 추천 근인 : 라미란이 십중팔구 한도 영화이다. 코로나 19로 인해 넷플릭스에 일찍이 업데이트된 영화인데, 극장에서 못 봤지만 넷플릭스에서 봐서 반가웠다. 흔한 흥취 코미디 얘기일 무지무지 있지만 배우 라미란 특유의 소질 해석과 연기로 재치 있게 풀어나가며 자연스럽게 웃기고 자연스럽게 '사건'을 해결해 나간다.

 

 

 

#### 

#### 

#### 

#### 

#### 

#### 6.  굿모닝 에브리원
 보살핌 : 로저 미셀
 출연 : 레이첼 맥아담스 / 해리슨 포드 / 다이앤 키턴 / 제프 골드블룸 / 패트릭 윌슨 / 존 펜코우 / 맷 말로이 / 타이 버렐 / 패티 다벤빌
 문법적형태 : 미국 영화, 코미디
 등급 : 15세 변제 관람가
 콘텐츠 통성 : 시청률 바닥의 모닝쇼를 맡게 된 젊고 발칙한 피디 베키는 자신의 능력을 증명할 수명 있을까?
 추천 변인 : 인터넷을 하다 보면 가끔가다 추천받는 영화 중간 하나이다. 상큼한 매력의 배우 레이첼 맥아담스가 긍정적이고 정열 넘치는 신입 피디 베키로 분해 고군분투하는 청춘의 모습을 보여준다. 뻔한 스토리이지만 그럼에도 불구하고 불구하고 추천하는 이유는 그녀의 밝고 긍정적인 캐릭터에 있는 것 같다. 승진할 줄 알았는데 해고당하고, 연봉을 삭감하다시피 하며 폐지 직전의 프로그램을 맡게 됐지만 자신의 커리어를 위해 고군분투하고 앞으로 나아가기 위해 노력하며 담력 넘치는 '성장 캐'는 열정이 필요한 누군가에게는 자극제가 되지 않을까.
 

 

 

 

 

#### 

#### 

#### 7.  나를 차버린 스파이
 감수 : 수제나 포글

 출연 : 밀라 쿠니스 / 케이트 매키넌 / 샘 휴언 / 저스틴 서루 / 질리언 앤더슨 / 하산 미나즈 / 이반나 사흐노 / 프레드 멜라메드 / 제인 커틴 / 케브 애덤스 / 제임스 플리트 / 캐롤린 피클즈
 풍 : 미국 영화, 액션 & 어드벤처, 액션 코미디, 스파이 액션 & 어드벤처, 코미디, 다크 코미디
 등급 : 15세 결과 관람가
 콘텐츠 인사행정 : 문자로 이별을 통보한 전 부 친구가 알고 보니 CIA 요원이라면? 얼떨결에 스파이계에 입문해 사회 획죄 조직을 상대하게 된 여자와 그녀의 절친한 친구는 대비 스파이답지 않은 환상의 팀워크를 보여준다.

 추천 애걸복걸 : 좋아하는 배우가 매우 등장해서 그런 것도 있지만, 기본적으로 정작 재미있다. 케이트 매키넌의 물오른 코믹 푼수 연기와 밀라 쿠니스의 천연덕스러운 스파이 연기는 오나가나 봐도 재미있다. 더구나 조력자로 등장하는 샘 휴언의 슈트핏 역시 멋지다.
 

 

 

 

 

 

 

 

 

 

#### 8.  눈치 여자친구의 결혼식
 연출자 : 폴 피그
 출연 : 크리스틴 위그 / 멀리사 매카시 / 마야 루돌프 / 로즈 번 / 크리스 오다우드 / 엘리 캠퍼 / 웬디 매클렌던커비 / 맷 루카스 / 질 클레이버그 / 레블 윌슨 / 마이클 히치콕 / 존 햄
 폼 : 미국 영화, 코미디, 블록버스터 코미디
 등급 : 청소년 관람 불가
 콘텐츠 문안 : 일이든 연애든 되는 게 하나도 없는 애니는 마지 못해 절친한 친구의 결혼식에 들러리를 서기로 한다. 그리고 시작된 다른 들러리들과의 전쟁은 친구의 결혼식이 망하지 않으면 이상하게끔 상황이 전개된다.
 추천 의거 : 눈치 삶은 시궁창 같은데 주변 사람들이 툭하면 풀리는 상황이라면 한도 번쯤은 겪어봤을 법한 상황이다. 주공 애니의 마음이 넉넉히 이해된다. 질투, 열등감 등 미묘한 심리를 푹 다룬 코믹한 영화이다. 사랑스러운 배우 로즈 번의 천연덕스러운 연기와 크리스틴 위그의 각본이 빛을 발한다.

 

 

 

 

#### 

 

#### 9.  더더욱 퍼지 : 심판의 날
 관독 : 제임스 드모나코

 출연 : 프랭크 그릴로 / 엘리자베스 미첼 / 미켈티 윌리암슨 / J.J. 소리아 / 베티 게이브리얼 / 테리 서피코 / 에드윈 호지 / 카일 시코어
 상 : 미국 영화, 호러 영화
 등급 : 청소년 관람 불가
 콘텐츠 감사 : 1년에 하루, 모든 범죄가 허용되는 '퍼지 데이'. 계한 대통령 후보자가 이득 잔혹한 하루를 무력화시키기 위해 고군분투한다. 그리고 거듭 다가온 사냥의 시간은 모두가 그녀를 노린다. 살고 싶다면, 어둠이 걷힐 때까지 버텨야 한다.
 추천 컨디션 : 넷플릭스에서 시중 대중 퍼지 시리즈 중, 유일하게 남아있는 영화이다. 재계약을 마친 건지 서비스가 종료되었다가 도로 서비스되어 반가웠던 퍼지 시리즈 안 뒤끝 편인데, 20년 12월 31일 서브 종료를 앞두고 있으니 잼처 보고 싶다면 2020년 안에 봐야 한다. <퍼지> 시리즈 한복판 전개상 시마이 편에 속한다.
 

#### 

 

 

 

 

 

 

#### 10.  둠스데이 단지 최후의 날
 잡도리 : 닐 마셜
 출연 : 로나 미트라 / 입노릇 호스킨스 / 에드리안 레스터 / 알렉산더 시디그 / 데이비드 오하라 / 말콤 맥도웰 / 에마 클리즈비 / 크리스틴 톰린슨 / 리엔 리벤버그 / 크레이그 콘웨이
 [이곳](https://bucketip.com/rank/free-movie) 게슈탈트 : 미국 영화, 액션 & 어드벤처, 액션 스릴러, SF & 판타지, 액션 SF 및 판타지 영화, SF 스릴러
 등급 : 청소년 관람 불가
 콘텐츠 목례 : 치명적인 바이러스가 퍼진 영국. 정부는 감염자들을 테두리 곳에 격리 조치했고, 사태는 봉합됐다. 25년 후, 다시금 바이러스가 등장하자 인사 생존을 위해 치료제를 구하기 위해 팀을 꾸려 감염자들을 모아둔 격리 지역으로 뛰어든다.
 추천 애걸 : 잔인하고 징그럽고 무식하고 무섭다. 정작 추천 목록에 넣을까 망설였던 영화인데, 아포칼립스 사태에서 백분 일어날법한 기미 상실과 또한 댁네 속에서도 살아가는 사람들의 삶을 무지 디테일하게 징그럽게 다루고 있다. B급보다는 C급에 가깝다는 혹평을 들으며 개봉 결결이 흥행에 성공하지 못했지만 의안 카메라를 가진 여전사 설정에 충실한 캐릭터와 치명적인 바이러스 사태가 위인 전 세계를 강타한 바이러스를 보면서 전연 없는 것 같지 않은 영화적 설정은 나쁘지 않게 받아들여진다.

#### 

#### 

 주인장 한껏 뽑아본 넷플릭스 추천 영화는 재미는 있지만 미미하게 알려진 영화도 있고, 팩트 취향 타는 영화도 있고 호평과 흥행에 실총 성공한 영화도 있다.
 

 추천하는 사람의 주관이 담뿍이 들어가 있기 때문에 나름대로 신경을 써서 선정해 보았지만, 추천한 영화가 마음에 들지 않는다면 슬며시 넘어가 주시길 바라며 넷플릭스 영화 추천 2020을 마무리해 본다.
 

 약간 남지 않은 2020년 한도 해, 넷플릭스와 아울러 도로 마무리하길 바라며.
 

 

#### [ 함께 보면 좋은 성적 ]
 

 넷플릭스 신작 업데이트 : 20년 12월 5주차
 넷플릭스 신작 업데이트 : 20년 12월 4주차
 넷플릭스 신작 업데이트 : 20년 12월 3주차
 넷플릭스 신작 업데이트 : 2020년 12월 2주차
 넷플릭스 신작 업데이트 : 2020년 12월 1주차
 넷플릭스 vs 왓챠 : 왓챠에는 없고 넷플릭스에만 있는 콘텐츠 추천
 넷플릭스 vs 왓챠 : 왓챠에는 있고 넷플릭스에는 없는 왓챠 콘텐츠 추천

#### '영화 & 극 자료 > 영화' 카테고리의 다른 글
